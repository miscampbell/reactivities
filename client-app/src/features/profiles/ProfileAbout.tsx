import React, { useContext, useState } from "react";
import { RootStoreContext } from "../../app/stores/rootStore";
import { Grid, Header, Button, Tab } from "semantic-ui-react";
import { IProfile } from "../../app/models/Profile";
import ProfileEditForm from "./ProfileEditForm";

const ProfileAbout = () => {
  const rootStore = useContext(RootStoreContext);
  const { profile, isCurrentUser, updateProfile } = rootStore.profileStore;

  const [updateAboutMode, setUpdateAboutMode] = useState(false);

  const handleUpdateProfile = (profile: Partial<IProfile>) => {
    updateProfile(profile).then(() => {
      setUpdateAboutMode(false);
    });
  };

  return (
    <Tab.Pane>
      <Grid>
        <Grid.Column width={16} style={{ paddingBottom: 0 }}>
          <Header floated="left" icon="user" content={profile?.displayName} />
          {isCurrentUser && (
            <Button
              floated="right"
              basic
              content={updateAboutMode ? "Cancel" : "Edit"}
              onClick={() => setUpdateAboutMode(!updateAboutMode)}
            />
          )}
        </Grid.Column>
        <Grid.Column width={16}>
          {updateAboutMode ? (
            <ProfileEditForm updateProfile={handleUpdateProfile} profile={profile!} />
          ) : (
            <span>{profile!.bio}</span>
          )}
        </Grid.Column>
      </Grid>
    </Tab.Pane>
  );
};

export default ProfileAbout;
