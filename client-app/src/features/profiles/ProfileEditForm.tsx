import React from "react";
import { IProfile } from "../../app/models/Profile";
import { Form as FinalForm, Field } from "react-final-form";
import { Form, Button } from "semantic-ui-react";
import { isRequired, combineValidators } from "revalidate";
import TextInput from "../../app/common/form/TextInput";
import TextAreaInput from "../../app/common/form/TextAreaInput";

const validate = combineValidators({
  displayName: isRequired({ message: "The display name is required" }),
});

interface IProps {
  updateProfile: (profile: IProfile) => void;
  profile: IProfile;
}

const ProfileEditForm: React.FC<IProps> = ({ updateProfile, profile }) => {
  return (
    <FinalForm
      validate={validate}
      initialValues={profile!}
      onSubmit={updateProfile}
      render={({ handleSubmit, invalid, pristine, submitting }) => (
        <Form onSubmit={handleSubmit}>
          <Field
            name="displayName"
            placeholder="Display Name"
            value={profile?.displayName}
            component={TextInput}
          />
          <Field
            name="bio"
            placeholder="Bio"
            value={profile?.bio}
            component={TextAreaInput}
          />
          <Button
            floated="right"
            positive
            type="submit"
            content="Update Profile"
            disabled={invalid || pristine}
            loading={submitting}
          />
        </Form>
      )}
    />
  );
};

export default ProfileEditForm;
